const question=document.getElementById('question')
const choices=Array.from(document.getElementsByClassName("choice-text"));
const questionNo=document.getElementById("progress-text")
const innerBar=document.getElementById("progress-bar-fill")
const Finalscore=document.getElementById("score")

let currentQuestion={};
let acceptAnswers=false;
let score=0;
let questionCounter=0
let availableQuestions=[];
let questions=[];
fetch("https://opentdb.com/api.php?amount=10&category=9&difficulty=easy&type=multiple")
.then(res=>{
    return res.json()
}) 
.then(loadedQuestions=>{
    // console.log(loadQuestions)
    questions=loadedQuestions.results.map(loadQuestions=>{
        const formattedQuestions={
            question: loadQuestions.question
        };
        const answerChoices=[...loadQuestions.incorrect_answers]
        console.log("answerchoices",answerChoices)
        formattedQuestions.answer=Math.floor(Math.random()*3)+1;
        answerChoices.splice(
            formattedQuestions.answer-1,
            0,
            loadQuestions.correct_answer
        );
        answerChoices.forEach((elem,index)=>{
            formattedQuestions["choice"+(index+1)]=elem;
        });
        return formattedQuestions;

    });
    startgame();

}) 
.catch(err=>{
    console.log(err);
})

//constants//
const CORRECT_BONUS=10
const MAX_QUESTIONS=5

startgame=()=>{
    questionCounter=0;
    score=0;
    availableQuestions=[...questions]
    // console.log(availableQuestions);
    getNewQuestion();
}
getNewQuestion=()=>{
    if(availableQuestions.length===0||questionCounter>=MAX_QUESTIONS){
        //go to some other page//
        return window.location.assign("result.html")
    }

    questionCounter++;
    // console.log(`${questionCounter}/${MAX_QUESTIONS}`)
    questionNo.innerText=`Question ${questionCounter}/${MAX_QUESTIONS}`
    innerBar.style.width=(questionCounter/MAX_QUESTIONS*100)+"%"
    const quetionIndex=Math.floor(Math.random()*availableQuestions.length)
    currentQuestion=availableQuestions[quetionIndex]
    // console.log(currentQuestion)
    question.innerText=currentQuestion.question;

    choices.forEach(choice=>{
        const number=choice.dataset['number']
        choice.innerText=currentQuestion['choice'+number]

    })
    availableQuestions.splice(quetionIndex,1);
    acceptAnswers=true;
}
choices.forEach(choice=>{
    choice.addEventListener('click',e=>{
        // console.log(e.target)
        if(!acceptAnswers) return;
        acceptAnswers=false;
        const selectedChoice=e.target;
        // console.log(selectedChoice)
        const selectedAnswer=selectedChoice.dataset["number"];
        // console.log(selectedAnswer)
        // if(selectedAnswer==currentQuestion.answer){
        //     const classToApply="correct"
        // }
        // else{
        //     const classToApply="correct"
        // }
        const classToApply=selectedAnswer==currentQuestion.answer?'correct':'incorrect'
        if (classToApply==="correct"){
            incrementScore(CORRECT_BONUS)

        }
        selectedChoice.classList.add(classToApply)
        setTimeout(()=>{
            selectedChoice.classList.remove(classToApply)
            getNewQuestion()
        },1000)
        // console.log(classToApply)
       
    })
})
incrementScore=(num)=>{
    // score+=num
    score=score+num;
    Finalscore.innerText=score
    localStorage.setItem('finalScore',score)
}
function resetLocalStorage(){
    localStorage.clear();
}
